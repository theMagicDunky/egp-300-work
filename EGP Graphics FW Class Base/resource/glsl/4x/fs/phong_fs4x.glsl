/*
	Phong (FS)
	By Dan Buckstein
	Fragment shader that computes Phong shading.
	
	Modified by: ______________________________________________________________
*/

//Duncan and Gabe did this

// version
#version 410

//ambient lighting
const vec3 ka = vec3(0.1, 0.1, 0.1);

// varyings
in vec2 passUV;
in vec4 passNormal;
in vec4 lightVec;
in vec4 viewerVec;

// uniforms
uniform sampler2D tex_dm;
uniform sampler2D tex_sm;

// target
layout (location = 0) out vec4 fragColor;

// shader function
void main()
{
	// ****
	// this example: phong shading algorithm
	vec4 N = normalize(passNormal);
	vec4 L = normalize(lightVec);
	vec4 V = normalize(viewerVec);

	//diffuse shading
	float kd = dot(N, L);
	vec4 R = (kd + kd)*N-L;

	//specular
	float ks = dot(V ,R);

	ks*=ks;
	ks*=ks;
	ks*=ks;
	ks*=ks;

	// output: phong
	vec4 sampleColor = texture(tex_dm, passUV);
	vec4 specularColor = texture(tex_sm, passUV);

	fragColor.rgb = vec3((sampleColor * kd) + (specularColor * ks)) + ka;
	//fragColor.a = 1.0;
}