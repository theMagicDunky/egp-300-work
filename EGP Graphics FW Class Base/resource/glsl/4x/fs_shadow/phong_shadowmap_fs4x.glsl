/*
	Phong with Shadow Mapping
	By Dan Buckstein
	Fragment shader that performs Phong shading and shadow mapping.
	
	Modified by: ______________________________________________________________
*/

#version 410


// ****
// varyings: 
in vec4 position_clip_alt;
in vec4 passNormal;
in vec4 lightVec;
in vec4 viewerVec;
in vec4 passUV;

// ****
// uniforms: 
uniform sampler2D shadowmap;
uniform sampler2D tex_dm;
uniform sampler2D tex_sm;

// target: 
layout (location = 0) out vec4 fragColor;


// ****
// constants/globals: 
const vec3 ka = vec3(0.1, 0.1, 0.1);


void main()
{
	vec4 N = normalize(passNormal);
	vec4 L = normalize(lightVec);
	vec4 V = normalize(viewerVec);

	//diffuse shading
	float kd = dot(N, L);
	vec4 R = (kd + kd)*N-L;

	//specular
	float ks = dot(V ,R);

	ks = max(0.0, ks);

	ks*=ks;
	ks*=ks;
	ks*=ks;
	ks*=ks;

	// output: phong
	vec4 sampleColor = texture(tex_dm, passUV.xy);
	vec4 specularColor = texture(tex_sm, passUV.xy);

	vec4 projector_screen = position_clip_alt / position_clip_alt.w;
	vec4 shadowSample = texture(shadowmap, projector_screen.xy);
	float shadowValue = shadowSample.r;
	float shadowDist = projector_screen.z;

	bool SHADOW = shadowDist > shadowValue + 0.001;

	vec3 phongColor = vec3((sampleColor * kd) + (specularColor * ks)) + ka;

	fragColor.rgb = phongColor * (SHADOW ? 0.25 : 1.0);

//debug
//	fragColor = vec4(0.0, 0.5, 1.0, 1.0);
//	fragColor = position_clip_alt;
//  fragColor = vec4(projector_screen.xy, 0.0, 0.0);
//	fragColor = shadowSample;
//	fragColor = vec4(shadowDist);
//	fragColor = vec4(shadowValue);
//	fragColor = normalize(viewerVec)*0.5 + 0.5;
//	fragColor = passUV;
	
}