/*
	Draw Curve - Morphing
	By Dan Buckstein
	Geometry shader that draws the path that a morphing vertex follows.
	
	Modified by: Duncan Carroll
*/

#version 410

// ****
//10 samples per segment
#define SAMPLES_MAX 10
//4 segments per curve
//3 curves because 3 verts
// total verts = min 10 * 4 * 3 = 120
#define VERTICES_MAX 128

// geometry type layout qualifiers
layout (triangles) in;
layout (line_strip, max_vertices = VERTICES_MAX) out;

// ****
// receive varying data
in vertexdata
{
	vec4 position0;
	vec4 position1;
	vec4 position2;
	vec4 position3;
} pass[];

// uniforms
uniform mat4 mvp;

// varying output: solid color
out vec4 passColor;


void drawLineFull(in vec4 p0, in vec4 p1)
{
	gl_Position = mvp * p0;
	EmitVertex();
	gl_Position = mvp * p1;
	EmitVertex();
	EndPrimitive();
}

// ****
// LERP
vec4 lerp(in vec4 p0, in vec4 p1, const float t)
{
	return (p0 + (p1 - p0) * t);
}

// ****
// Catmull-Rom spline interpolation
vec4 sampleCatmullRom(in vec4 pPrev, in vec4 p0, in vec4 p1, in vec4 pNext, const float t)
{
	//kernel, influence matrix, time matrix
	const mat4 kernel = mat4(
		0.0, 2.0, 0.0, 0.0,
		-1.0, 0.0, 1.0, 0.0,
		2.0, -5.0, 4.0, -1.0,
		-1.0, 3.0, -3.0, 1.0) * 0.5;

	mat4 influence = mat4(pPrev, p0, p1, pNext);

	vec4 time = vec4(1.0, t, t*t, t*t*t);

	return (influence * kernel * time);
}

// ****
// cubic Hermite spline interpolation
vec4 sampleCubicHermite(in vec4 p0, in vec4 m0, in vec4 p1, in vec4 m1, const float t)
{
	mat4 kernel = mat4(
	1.0, 0.0, 0.0, 0.0,
	0.0, 1.0, 0.0, 0.0,
	-3.0, -2.0, 3.0, -1.0,
	2.0, 1.0,-2.0, 1.0);

	mat4 influence = mat4(p0, m0, p1, m1);

	vec4 time = vec4(1.0, t, t*t, t*t*t);

	return (influence * kernel * time);
}


void drawSegment(const int samples, const float dt,
	in vec4 p0, in vec4 p1, in vec4 p2, in vec4 p3)
{
	//Catmull - Rom
	vec4 pNext = p2;
	vec4 pPrev = p3;

	//Hermite
	vec4 m0 = p2 - p0;
	vec4 m1 = p3 - p1;

	int i = 0;
	float t = 0.0;
	for(; i < samples; ++i, t+= dt)
	{
		gl_Position = mvp * sampleCatmullRom(pPrev, p0, p1, pNext, t);
		EmitVertex();
	}
}

void drawVertexSegments(const int samples, const float dt,
	in vec4 p0, in vec4 p1, in vec4 p2, in vec4 p3)
{
	drawSegment(samples, dt, p0, p1, p2, p3);
	drawSegment(samples, dt, p1, p2, p3, p0);
	drawSegment(samples, dt, p2, p3, p0, p1);
	drawSegment(samples, dt, p3, p0, p1, p2);

	//complete loop by drawing first point
	gl_Position = mvp * p0;
	EmitVertex();
	EndPrimitive();
}

void drawCurves(const int samples, const float dt)
{
	drawVertexSegments(samples, dt, pass[0].position0, pass[0].position1, pass[0].position2, pass[0].position3);
	drawVertexSegments(samples, dt, pass[1].position0, pass[1].position1, pass[1].position2, pass[1].position3);
	drawVertexSegments(samples, dt, pass[2].position0, pass[2].position1, pass[2].position2, pass[2].position3);
}

void main()
{
	// ****
	// do line drawing
	const int samplesPerSegment = SAMPLES_MAX;
	const float dt = 1.0 / float(samplesPerSegment);

	// testing: pass solid color
	passColor = vec4(0.0, 0.5, 1.0, 1.0);

	drawCurves(samplesPerSegment, dt);


// DEBUGGING: draw a normal vector for each vertex
	//const vec4 n = vec4(0.0, 0.0, 1.0, 0.0);
	//drawLineFull(pass[0].position, pass[0].position + n);
	//drawLineFull(pass[1].position, pass[1].position + n);
	//drawLineFull(pass[2].position, pass[2].position + n);
}