/*
	Skinning & Pass Normal as Color
	By Dan Buckstein
	Vertex shader that performs vertex skinning, passing resulting normal out
		as a solid color for debugging purposes.
	
	Modified by: ______________________________________________________________
*/

#version 410


// ****
// the usual attributes, but also need skinning attributes: 
// weights and bone indices
layout (location = 0) in vec4 position;
layout (location = 2) in vec4 normal;


layout (location = 1) in vec4 blendWeight;
layout (location = 7) in vec4 blendIndices;

uniform mat4 mvp;


#define BONES_MAX 64

// ****
// skinning uniforms
uniform mat4 bones_skinning[BONES_MAX];

// pass some piece of data along for debugging
out vec4 passColor;


// ****
// skinning algorithm: 
// vector goes indeformed vector comes out
vec4 skin(in vec4 v)
{
	vec4 v_out = vec4(0.0);

	v_out += blendWeight[0] * bones_skinning[int(blendIndices[0])] * v;
	v_out += blendWeight[1] * bones_skinning[int(blendIndices[1])] * v;
	v_out += blendWeight[2] * bones_skinning[int(blendIndices[2])] * v;
	v_out += blendWeight[3] * bones_skinning[int(blendIndices[3])] * v;

	return v_out;
}


void main()
{
	// ****
	// skin any attribute that has to do with the SURFACE
	// i.e. anything that could potentially affect shading
	vec4 skinnedPosition = skin(position);
	vec4 skinnedNormal = skin(vec4(normal.xyz, 0));

	gl_Position = mvp * skinnedPosition;

	passColor = skinnedNormal*0.5+0.5;

	// pass debug color
	//passColor = normal * 0.5 + 0.5;
	//passColor = blendWeight;
	//passColor = vec4(blendIndices[0] * 0.25);
}