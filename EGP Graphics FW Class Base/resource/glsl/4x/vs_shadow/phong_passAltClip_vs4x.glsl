/*
	Phong - Pass Alt Clip Position
	By Dan Buckstein
	Vertex shader that performs vertex requirements for Phong shading, but also
		passes clip position of light for shadow mapping/projective texturing.
	
	Modified by: ______________________________________________________________
*/

#version 410


// ****
// attributes: 
layout (location = 0) in vec4 position;
layout (location = 2) in vec4 normal;
layout (location = 8) in vec4 texcoord;


// ****
// uniforms: 
uniform mat4 mvp;
uniform mat4 mvp_projector;
uniform mat4 atlasMat;
uniform vec4 lightPos;
uniform vec4 eyePos;
uniform float normalScale;

// ****
// varyings: 
out vec4 position_clip_alt;
out vec4 passNormal;
out vec4 passUV;
out vec4 lightVec;
out vec4 viewerVec;

void main()
{
	// set clip position 'gl_Position'
	gl_Position = mvp * position;

	position_clip_alt = mvp_projector * position;
	passUV = atlasMat * texcoord;
	passNormal = vec4(normal.xyz*normalScale, 0.0);
	lightVec = lightPos - position;
	viewerVec = eyePos - position;
}