// By Dan Buckstein
// Modified by: _______________________________________________________________
#include "egpfw/egpfw/egpfwInterpolation.h"


#include <math.h>


// ****
// lerp and lerp inverse
float egpfwLerp(const float v0, const float v1, const float param)
{
	//...
	return v0 + (v1 - v0) * param;
}

// ****
float egpfwLerpInv(const float v0, const float v1, const float v)
{
	//...
	return (v - v0) / (v1 - v0);
}


// ****
// Catmull-Rom spline interpolation
float egpfwCatmullRom(const float vPrev, const float v0, const float v1, const float vNext, const float param)
{
	//...
	float pPrev = (-param + 2 * param*param - param*param*param)*vPrev;
	float p0 = (2 - 5 * param*param + 3 * param*param*param)*v0;
	float p1 = (param + 4 * param*param - 3 * param*param*param)*v1;
	float p2 = (-param * param + param*param*param)*vNext;

	return .5f*(pPrev + p0 + p1 + p2);
}


// ****
// Cubic hermite spline interpolation
float egpfwCubicHermite(const float v0, const float dv0, const float v1, const float dv1, const float param)
{
	//...
	return 0.0f;
}


// Bezier spline interpolation
float egpfwBezier0(const float v0, const float param)
{
	// this function is complete!
	return v0;
}

// ****
float egpfwBezier1(const float v0, const float v1, const float param)
{
	//...
	return egpfwLerp(egpfwBezier0(v0, param), egpfwBezier0(v1, param), param);
}

// ****
float egpfwBezier2(const float v0, const float v1, const float v2, const float param)
{
	//...
	return egpfwLerp(egpfwBezier1(v0, v1, param), egpfwBezier1(v1, v2, param), param);
}

// ****
float egpfwBezier3(const float v0, const float v1, const float v2, const float v3, const float param)
{
	//...
	return egpfwLerp(egpfwBezier2(v0, v1, v2, param), egpfwBezier2(v1, v2, v3, param), param);
}

// ****
float egpfwBezier4(const float v0, const float v1, const float v2, const float v3, const float v4, const float param)
{
	//...
	return egpfwLerp(egpfwBezier3(v0, v1, v2, v3, param), egpfwBezier3(v1, v2, v3, v4, param), param);
}

// ****
float egpfwBezier(const float *v, unsigned int order, const float param)
{
	//...
	if (order == 0)
		return egpfwBezier0(v[0], param);
	else if (order == 1)
		return egpfwBezier1(v[0], v[1], param);
	else if (order == 2)
		return egpfwBezier2(v[0], v[1], v[2], param);
	else if (order == 3)
		return egpfwBezier3(v[0], v[1], v[2], v[3], param);

	return egpfwBezier4(v[0], v[1], v[2], v[3], v[4], param);
}


// ****
// vector interpolation
void egpfwLerpVector(const float *v0, const float *v1, const float param, const unsigned int numElements, float *v_out)
{
	//...
	for (int i = 0; i < numElements; ++i)
	{
		v_out[i] = egpfwLerp(v0[i], v1[i], param);
	}
}

// ****
void egpfwCatmullRomVector(const float *vPrev, const float *v0, const float *v1, const float *vNext, const float param, const unsigned int numElements, float *v_out)
{
	//...
	for (int i = 0; i < numElements; ++i)
	{
		v_out[i] = egpfwCatmullRom(vPrev[i], v0[i], v1[i], vNext[i], param);
	}
}

// ****
void egpfwCubicHermiteVector(const float *v0, const float *dv0, const float *v1, const float *dv1, const float param, const unsigned int numElements, float *v_out)
{
	//...
}

// ****
void egpfwBezierVector(const float *v, unsigned int order, const float param, const unsigned int numElements, float *v_out)
{
	//...
	for (int i = 0; i < numElements; ++i)
	{
		v_out[i] = egpfwBezier(v, order, param);
	}
}


// ****
// table sampling
unsigned int egpfwSearchSampleTable(const float *sampleTable, const float *paramTable, const float searchParam, unsigned int numElements, float *param_out)
{
	//...
	return 0;
}


// ****
// calculate arc length
float egpfwComputeArcLengthCatmullRom(const float *vPrev, const float *v0, const float *v1, const float *vNext, unsigned int numElements, unsigned int numSamples, int autoNormalize, float *prevSamplePtr, float *currentSamplePtr, float *sampleTable_out, float *paramTable_out)
{
	//...
	return 0.0f;
}

// ****
float egpfwComputeArcLengthCubicHermite(const float *v0, const float *dv0, const float *v1, const float *dv1, unsigned int numElements, unsigned int numSamples, int autoNormalize, float *prevSamplePtr, float *currentSamplePtr, float *sampleTable_out, float *paramTable_out)
{
	//...
	return 0.0f;
}

// ****
float egpfwComputeArcLengthBezier(const float *v, unsigned int order, unsigned int numElements, unsigned int numSamples, int autoNormalize, float *prevSamplePtr, float *currentSamplePtr, float *sampleTable_out, float *paramTable_out)
{
	//...
	return 0.0f;
}
