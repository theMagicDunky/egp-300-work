// By Dan Buckstein
// Modified by: Duncan Carroll, Gabriel Pereyra, and Lucas Spiker with permission from author

#include "egpfw/egpfw/egpfwOBJLoader.h"

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


#define BUFFER_OFFSET_BYTE(p,n) ((char *)(p) + n)

#define BUFFER_SIZE 1024

// helper structures
union float2
{
	struct { float f0, f1; };
	float f[2];
};
union float3
{
	struct { float f0, f1, f2; };
	float f[3];
};
union float4
{
	struct { float f0, f1, f2, f3; };
	float f[4];
};
union int4
{
	struct { int i0, i1, i2, i3; };
	int i[4];
};
union face
{
	struct { unsigned int v0, v1, v2, vt0, vt1, vt2, vn0, vn1, vn2; };
	struct { unsigned int v[3], vt[3], vn[3]; };
};

struct uniqueTriFace;
struct uniqueVertex;
struct uniqueTexcoord;
struct uniqueNormal;

#ifndef __cplusplus
typedef union float2			float2;
typedef union float3			float3;
typedef union float4			float4;
typedef union int4				int4;
typedef union face				face;
typedef struct uniqueTriFace	uniqueTriFace;
typedef struct uniqueVertex		uniqueVertex;
typedef struct uniqueTexcoord	uniqueTexcoord;
typedef struct uniqueNormal		uniqueNormal;
#endif	// __cplusplus


//-----------------------------------------------------------------------------

// Duncan, Lucas, and Gabe worked on this
// load triangulated OBJ file
egpTriOBJDescriptor egpfwLoadTriangleOBJ(const char *objPath, const egpMeshNormalMode normalMode, const double globalScale)
{
	egpTriOBJDescriptor obj = { 0 };

	void* uniquePositions, *uniqueTexcoords, *uniqueNormals, *tempUniquePositions, *tempUniqueTexcoords, *tempUniqueNormals;
	unsigned int numUniquePositions = 0, numUniqueTexcoords = 0, numUniqueNormals = 0, faceCount = 0, facePos = 0;

	FILE *file = fopen(objPath, "r");
	char buff[BUFFER_SIZE];

	if (file == NULL)
	{
		printf("Error: couldn't open file\n");
		return obj;
	}

	memset(obj.attribOffset, 0, 16 * sizeof(int));

	printf("\n");

	//first run through, determines data size
	while (fgets(buff, BUFFER_SIZE, file))
	{
		if (buff[0] == 'v')
		{
			if (buff[1] == ' ')
			{
				++numUniquePositions;
			}
			else if (buff[1] == 'n')
			{
				++numUniqueNormals;
			}
			else if (buff[1] == 't')
			{
				++numUniqueTexcoords;
			}
		}
		else if (buff[0] == 'f')
		{
			++faceCount;
		}
	}

	//all unique attributes are saved into their own arrays
	//then the face values are used to determine which order to put the unique attributes into the data array

	//allocating size for arrays of unique attributes and the data pointer in the obj
	obj.dataSize = (faceCount * 3 * (sizeof(float3) + sizeof(float2) + sizeof(float3))) + 4;
	obj.data = malloc(obj.dataSize);
	uniquePositions = malloc(numUniquePositions * sizeof(float3));
	uniqueTexcoords = malloc(numUniqueTexcoords * sizeof(float2));
	uniqueNormals = malloc(numUniqueNormals * sizeof(float3));

	//setting attribute offsets
	obj.attribOffset[ATTRIB_POSITION] = 4;
	obj.attribOffset[ATTRIB_TEXCOORD] = obj.attribOffset[ATTRIB_POSITION] + (faceCount * 3 * sizeof(float3));
	obj.attribOffset[ATTRIB_NORMAL] = obj.attribOffset[ATTRIB_TEXCOORD] + (faceCount * 3 * sizeof(float2));

	tempUniquePositions = uniquePositions;
	tempUniqueTexcoords = uniqueTexcoords;
	tempUniqueNormals = uniqueNormals;

	//setting num of verticies
	*(unsigned int*)obj.data = faceCount * 3;

	//second run through, saves data in data*
	void* tempData = obj.data;	

	//start reading the file again to grab information
	rewind(file);

	//loop through file to get attribute information
	while (fgets(buff, BUFFER_SIZE, file))
	{
		if (buff[0] == 'v')
		{
			//if position
			if (buff[1] == ' ')
			{
				float3 vertex;

				int i = 2, j = 0, w = 0;
				char toFloat[BUFFER_SIZE];

				while (buff[i] != '\n')
				{
					if (buff[i] == ' ')
					{
						vertex.f[j++] = atof(toFloat);
						memset(toFloat, 0, BUFFER_SIZE);
						w = 0;
					}
					else
					{
						toFloat[w] = buff[i];
						++w;
					}

					++i;
				}

				vertex.f[j] = atof(toFloat);

				*(float3*)(tempUniquePositions) = vertex;
				tempUniquePositions = (char *)tempUniquePositions + sizeof(float3);
			}
			//if tex coord
			else if (buff[1] == 't')
			{
				float2 vertex;

				int i = 3, j = 0, w = 0;
				char toFloat[BUFFER_SIZE];

				while (buff[i] != '\n')
				{
					if (buff[i] == ' ')
					{
						vertex.f[j++] = atof(toFloat);
						memset(toFloat, 0, BUFFER_SIZE);
						w = 0;
					}
					else
					{
						toFloat[w] = buff[i];
						++w;
					}

					++i;
				}

				vertex.f[j] = atof(toFloat);

				*(float2*)(tempUniqueTexcoords) = vertex;
				tempUniqueTexcoords = (char *)tempUniqueTexcoords + sizeof(float2);
			}
			//if normal
			else if (buff[1] == 'n')
			{
				float3 vertex;

				int i = 3, j = 0, w = 0;
				char toFloat[BUFFER_SIZE];

				while (buff[i] != '\n')
				{
					if (buff[i] == ' ')
					{
						vertex.f[j++] = atof(toFloat);
						memset(toFloat, 0, BUFFER_SIZE);
						w = 0;
					}
					else
					{
						toFloat[w] = buff[i];
						++w;
					}

					++i;
				}

				vertex.f[j] = atof(toFloat);

				*(float3*)(tempUniqueNormals) = vertex;
				tempUniqueNormals = (char *)tempUniqueNormals + sizeof(float3);
			}
		}
		//if face 
		/*Lucas did reading of faces*/
		else if (buff[0] == 'f')
		{
			face tmpFace;
			
			int buffIndex = 2;
			int spacer = 2;
			int tmp = 0;
			char toInt[BUFFER_SIZE];

			while (buff[buffIndex] != '/')
			{
				++buffIndex;
			}
			for (int i = 0; i < buffIndex - spacer; i++)
			{
				toInt[i] = buff[i + spacer];
			}
			tmp = atoi(toInt);
			tmpFace.v0 = tmp;
			memset(toInt, 0, BUFFER_SIZE);
			++buffIndex;

			spacer = buffIndex;
			while (buff[buffIndex] != '/')
			{
				++buffIndex;
			}
			for (int i = 0; i < buffIndex - spacer; i++)
			{
				toInt[i] = buff[i + spacer];
			}
			tmp = atoi(toInt);
			tmpFace.vt0 = tmp;
			memset(toInt, 0, BUFFER_SIZE);
			++buffIndex;

			spacer = buffIndex;
			while (buff[buffIndex] != ' ')
			{
				++buffIndex;
			}
			for (int i = 0; i < buffIndex - spacer; i++)
			{
				toInt[i] = buff[i + spacer];
			}
			tmp = atoi(toInt);
			tmpFace.vn0 = tmp;
			memset(toInt, 0, BUFFER_SIZE);
			++buffIndex;

			spacer = buffIndex;
			while (buff[buffIndex] != '/')
			{
				++buffIndex;
			}
			for (int i = 0; i < buffIndex - spacer; i++)
			{
				toInt[i] = buff[i + spacer];
			}
			tmp = atoi(toInt);
			tmpFace.v1 = tmp;
			memset(toInt, 0, BUFFER_SIZE);
			++buffIndex;

			spacer = buffIndex;
			while (buff[buffIndex] != '/')
			{
				++buffIndex;
			}
			for (int i = 0; i < buffIndex - spacer; i++)
			{
				toInt[i] = buff[i + spacer];
			}
			tmp = atoi(toInt);
			tmpFace.vt1 = tmp;
			memset(toInt, 0, BUFFER_SIZE);
			++buffIndex;

			spacer = buffIndex;
			while (buff[buffIndex] != ' ')
			{
				++buffIndex;
			}
			for (int i = 0; i < buffIndex - spacer; i++)
			{
				toInt[i] = buff[i + spacer];
			}
			tmp = atoi(toInt);
			tmpFace.vn1 = tmp;
			memset(toInt, 0, BUFFER_SIZE);
			++buffIndex;

			spacer = buffIndex;
			while (buff[buffIndex] != '/')
			{
				++buffIndex;
			}
			for (int i = 0; i < buffIndex - spacer; i++)
			{
				toInt[i] = buff[i + spacer];
			}
			tmp = atoi(toInt);
			tmpFace.v2 = tmp;
			memset(toInt, 0, BUFFER_SIZE);
			++buffIndex;

			spacer = buffIndex;
			while (buff[buffIndex] != '/')
			{
				++buffIndex;
			}
			for (int i = 0; i < buffIndex - spacer; i++)
			{
				toInt[i] = buff[i + spacer];
			}
			tmp = atoi(toInt);
			tmpFace.vt2 = tmp;
			memset(toInt, 0, BUFFER_SIZE);
			++buffIndex;

			spacer = buffIndex;
			while (buff[buffIndex] != '\n')
			{
				++buffIndex;
			}
			for (int i = 0; i < buffIndex - spacer; i++)
			{
				toInt[i] = buff[i + spacer];
			}
			tmp = atoi(toInt);
			tmpFace.vn2 = tmp;
			memset(toInt, 0, BUFFER_SIZE);

			/*end of reading faces*/			

			//populate data here
			//read indicies from faces and add corresponding unique attributes to the obj data array
			*(float3*)((char*)obj.data + obj.attribOffset[ATTRIB_POSITION] + (sizeof(float3) * facePos)) = *(float3*)((char*)uniquePositions + sizeof(float3)*(tmpFace.v0 - 1));
			*(float2*)((char*)obj.data + obj.attribOffset[ATTRIB_TEXCOORD] + (sizeof(float2) * facePos)) = *(float2*)((char*)uniqueTexcoords + sizeof(float2)*(tmpFace.vt0 - 1));
			*(float3*)((char*)obj.data + obj.attribOffset[ATTRIB_NORMAL] + (sizeof(float3) * facePos)) = *(float3*)((char*)uniqueNormals + sizeof(float3)*(tmpFace.vn0 - 1));

			++facePos;

			*(float3*)((char*)obj.data + obj.attribOffset[ATTRIB_POSITION] + (sizeof(float3) * facePos)) = *(float3*)((char*)uniquePositions + sizeof(float3)*(tmpFace.v1 - 1));
			*(float2*)((char*)obj.data + obj.attribOffset[ATTRIB_TEXCOORD] + (sizeof(float2) * facePos)) = *(float2*)((char*)uniqueTexcoords + sizeof(float2)*(tmpFace.vt1 - 1));
			*(float3*)((char*)obj.data + obj.attribOffset[ATTRIB_NORMAL] + (sizeof(float3) * facePos)) = *(float3*)((char*)uniqueNormals + sizeof(float3)*(tmpFace.vn1 - 1));

			++facePos;

			*(float3*)((char*)obj.data + obj.attribOffset[ATTRIB_POSITION] + (sizeof(float3) * facePos)) = *(float3*)((char*)uniquePositions + sizeof(float3)*(tmpFace.v2 - 1));
			*(float2*)((char*)obj.data + obj.attribOffset[ATTRIB_TEXCOORD] + (sizeof(float2) * facePos)) = *(float2*)((char*)uniqueTexcoords + sizeof(float2)*(tmpFace.vt2 - 1));
			*(float3*)((char*)obj.data + obj.attribOffset[ATTRIB_NORMAL] + (sizeof(float3) * facePos)) = *(float3*)((char*)uniqueNormals + sizeof(float3)*(tmpFace.vn2 - 1));

			++facePos;
		}
	}

	//free unique data since the main obj data array has been populated
	free(uniquePositions);
	uniquePositions = NULL;
	free(uniqueTexcoords);
	uniqueTexcoords = NULL;
	free(uniqueNormals);
	uniqueNormals = NULL;

	return obj;
}


//Duncan and Gabe did this
// convert OBJ to VAO & VBO
int egpfwCreateVAOFromOBJ(const egpTriOBJDescriptor *obj, egpVertexArrayObjectDescriptor *vao_out, egpVertexBufferObjectDescriptor *vbo_out)
{
	void* posData, *texData, *normalData;

	//creating arrays for each type of attribute for the attribute descriptor array
	posData = malloc(sizeof(float3) * egpfwGetOBJNumVertices(obj));
	memcpy(posData, (char*)obj->data + obj->attribOffset[ATTRIB_POSITION], sizeof(float3) * egpfwGetOBJNumVertices(obj));

	texData = malloc(sizeof(float2) * egpfwGetOBJNumVertices(obj));
	memcpy(texData, (char*)obj->data + obj->attribOffset[ATTRIB_TEXCOORD], sizeof(float2) * egpfwGetOBJNumVertices(obj));

	normalData = malloc(sizeof(float3) * egpfwGetOBJNumVertices(obj));
	memcpy(normalData, (char*)obj->data + obj->attribOffset[ATTRIB_NORMAL], sizeof(float3) * egpfwGetOBJNumVertices(obj));
	
	//fill attribute array for vao creation
	egpAttributeDescriptor attribs[] = {
		egpCreateAttributeDescriptor(ATTRIB_POSITION, ATTRIB_VEC3, posData),
		egpCreateAttributeDescriptor(ATTRIB_NORMAL, ATTRIB_VEC3, normalData),
		egpCreateAttributeDescriptor(ATTRIB_TEXCOORD, ATTRIB_VEC2, texData),
	};
	
	//create vao
	*vao_out = egpCreateVAOInterleaved(PRIM_TRIANGLES, attribs, 3, egpfwGetOBJNumVertices(obj), vbo_out, 0);
	return 0;
}


// ****
// free obj data
int egpfwReleaseOBJ(egpTriOBJDescriptor *obj)
{
	free(obj->data);
	return 0;
}


// **** Lucas did this
// save/load binary
int egpfwSaveBinaryOBJ(const egpTriOBJDescriptor *obj, const char *binPath)
{
	
	//Open our file for writing
	FILE *file = fopen(binPath, "w");

	//Write the 16 offsets to the file
	fwrite(obj->attribOffset, sizeof(unsigned int), 16, file);
	//Write the datasize to the file
	fwrite(&obj->dataSize, sizeof(unsigned int), 1, file);
	//Write all of the data to the file
	fwrite(obj->data, obj->dataSize, 1, file);

	//close the file
	fclose(file);

	return 0;
}

// **** Lucas did this
egpTriOBJDescriptor egpfwLoadBinaryOBJ(const char *binPath)
{
	//Create a blank OBJ
	egpTriOBJDescriptor obj = { 0 };
	egpTriOBJDescriptor obj2 = { 0 };
	
	//Open the file for reading
	FILE *file = fopen(binPath, "r");

	if (!file)
	{
		return obj;
	}

	//Set up temporary variables to hold things
	unsigned int tmpAttribOffset[16];
	unsigned int tmpDataSize;
	void* tmpData;

	//Read the attribute offsets and data size 
	fread(&tmpAttribOffset, sizeof(unsigned int), 16, file);
	fread(&tmpDataSize, sizeof(unsigned int), 1, file);

	//Prep the space for, then read, the data buffer
	tmpData = malloc(tmpDataSize);
	fread(tmpData, tmpDataSize, 1, file);

	//close the file
	fclose(file);

	//Copy/point all of the data we've read into the obj
	for (int i = 0; i < 16; i++)
	{
		obj.attribOffset[i] = tmpAttribOffset[i];
	}
	obj.dataSize = tmpDataSize;

	obj.data = tmpData;

	return obj2;
}


// these functions are complete!
// get attribute data from OBJ
const void *egpfwGetOBJAttributeData(const egpTriOBJDescriptor *obj, const egpAttributeName attrib)
{
	if (obj && obj->data && obj->attribOffset[attrib])
		return BUFFER_OFFSET_BYTE(obj->data, obj->attribOffset[attrib]);
	return 0;
}

unsigned int egpfwGetOBJNumVertices(const egpTriOBJDescriptor *obj)
{
	if (obj && obj->data)
		return *((unsigned int *)obj->data);
	return 0;
}